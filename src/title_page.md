\begin{center}

{\Huge \textbf{Rapport TP2}} \\[2.5in]

{\Large Université Laval} \\[0.25in]
{\large IFT-2102} \\[0.25in]
{\large Été 2018} \\[2.5in]

{\large Par} \\[0.25in]
{\Large \textbf{Samuel Carpentier}} \\[0.25in]
{\Large 111 096 688}
\end{center}

\pagebreak
