# Rapport

Note au correcteur, les images sont petites dans le rapport, mais sont de qualité suffisante pour permettre de _zoomer_. Je vais tout de même joindre les images dans le ZIP de remise pour éviter de potentiels problèmes lors de la correction. Faites-moi signe s'il y a quoi que ce soit d'incorrect avec les images.

## 1. Identifier trois menaces qui s'appliquent à la solution

### 1. La sécurité du réseau et des diverses communications via l'internet

* L'envoi des trames GPS par le module sur chaque véhicule
* Leur réception sur le serveur d'écoute JAVA
* Les communications (requêtes-réponses) entre le serveur web et le UI web

### 2. La gestion des droits d'accès aux données via le UI web

* Exposition sur l'internet
* Présence de données confidentielles

### 3. La disponibilité du service

* Disponibilité du serveur d'écoute JAVA
* Disponibilité et redondance de la base de données
* Disponibilité du serveur web et du UI web

\pagebreak

## 2. Élaborer un arbre d’attaque pour chacune des menaces identifiées dans la question 1

### 1. La sécurité du réseau et des diverses communications via l'internet

![Arbre d'attaque - sécurité réseau](img/vulnerabilite1.png)

\pagebreak

### 2. La gestion des droits d'accès aux données via le UI web

![Arbre d'attaque - droits d'accès](img/vulnerabilite2.png)

\pagebreak

### 3. La disponibilité du service

![Arbre d'attaque - disponibilité du service](img/vulnerabilite3.png)

\pagebreak

## 3. Proposer une solution pour mitiger chacune des menaces identifiée

### 1. La sécurité du réseau et des diverses communications via l'internet

Selon le schéma du système et les informations détaillés dans le devis, il semblerait que le maillon le plus faible du point de vue des communications et du réseau soit l'absence de sécurité apparente au niveau des serveurs internes du système.

En effet, puisque ceux-ci sont exposés sur l'internet, il est primordial de les placer derrière un pare-feu. Également, le serveur web pourrait être placé dans une zone démilitarisée (DMZ), car il est potentiellement le serveur le plus à risque en raison de sa nature de serveur web. De cette façon, le réseau internet serait beaucoup moins vulnérable aux intrusions, qui pourraient influencer l'intégrité des données stockées dans la base de données.

Aussi, les communications entre les modules GPS et le serveur d'écoute JAVA pourraient utiliser du _2-way_ SSL. Il serait possible de procéder ainsi puisque tous les clients (modules GPS) qui feront des appels à ce serveur sont connus et peuvent donc préalablement avoir été configurés pour utiliser un certificat client et faire confiance (_trust_) au certificat serveur. Ainsi, la possibilité d'interception des requêtes vers le serveur d'écoute JAVA se verrait grandement réduite et la position en temps réel des véhicule demeurerait confidentielle.

Finalement, afin de mitiger le risque d'interception d'informations entre le UI web et le serveur associé, un canal de communication SSL pourrait également être utilisé. Le risque qu'un individu puisse _sniffer_ le traffic réseau et obtenir des informations sensibles serait largement mitigé.

### 2. La gestion des droits d'accès aux données via le UI web

Le moyen le plus facile pour mitiger tous les risques présents dans l'arbre d'attaque de cette vulnérabilité est d'intégrer l'authentification multi-facteurs. De cette façon, il devient beaucoup plus difficile pour un individu de voler les identifiants d'un utilisateur autorisé, puisque d'autres facteurs d'authentification seront requis pour obtenir l'accès.

Le cas de l'utilisation de la force brute est également fortement atténué pour les mêmes raisons.

D'une autre côté, il pourrait être judicieux d'utiliser un service d'authentification et d'autorisation reconnu comme Kerberos ou SAML, plutôt que de tenter d'implémenter une solution sur mesure. Ces services sont reconnus, largement utilisés et testés, en plus d'être certifiés.

Il est à noter que le serveur d'authentification devrait se retrouver derrière le pare-feu énoncé au point précédent.

### 3. La disponibilité du service

Les cibles les plus probables lors d'une attaque touchant la disponibilité du service sont le UI et le serveur web. Il est donc nécessaire de les protéger adéquatement contre ce type de menaces. Ceci dit, une attaque sur le serveur d'écoute JAVA ou la base de données aurait des impacts beaucoup plus important pour l'entreprise, qui risquerait inévitablement de perdre de précieuses données.

La mise en place d'un système de détection des intrusions (IDS) sur le réseau privé pourrait être judicieuse, conjointement avec l'utilisation d'un _honey pot_, qui tromperait les attaquants et, par le fait même, permettrait d'ammasser des métriques en lien avec le type d'attaques, leur fréquence et les moyens utilisés. Il serait ensuite possible de mieux juger des besoins du système en matière de protection contre les intrusions.

Finalement, l'implantation de redondance au niveau de la base de données, du serveur d'écoute JAVA et du serveur web pourrait permettre d'augmenter la disponibilité du service et d'assurer une solution de rechange en cas de panne. Chaque instance pourrait être placée derrière un répartiteur de charge (_load balancer_), qui diviserait le traffic équitablement entre celles-ci. Des copies de sauvegarde géo-distribuées du contenu de la base de données assureraient également l'intégrité des données en cas de sinistre local.

\pagebreak

## 4. Revue de code

### a. Recherche d’un véhicule dans la base de données

* Vulnérabilité: Risque d'injection SQL dans si la variable `vehicleid`.
* Solution: Effectuer des validations sur la variable `vehicleid` avant d'effectuer la requête SQL.

### b. Intégrité des opérations avec des nombres à virgule flottante

* Vulnérabilité:
    * Risque d'obtenir un _underflow_ ou un _overflow_ du tampon (buffer).
    * Risque de laisser une entrée ouverte par accident dans l'application lors de l'erreur de tampon.
* Solution:
    * Utiliser des types avec de plus grands espaces alloués (`long`).
    * Utiliser des librairies qui promeuvent le _type safety_.
    * Implémenter une gestion des erreurs serrée autour de ce code.
    * S'assurer de planter correctement et de ne pas laisser de trous de sécurité en cas d'échec.

### c. Chargement d’une librairie

* Vulnérabilité: La librairie externe est chargée en mode privilégié et pourrait exécuter du code malicieux.
* Solution:
    * Charger la librairie avec le minimum de privilèges possible.
    * De façon plus extrême (mais moins réaliste), ne pas utiliser de librairie externe.
    * Utiliser uniquement des librairies éprouvées et digne de confiance.

### d. Recherche utilisateur dans un LDAP

* Vulnérabilité:
    * Le mot de passe de l'utilisateur est passé en clair et est utilisé comme filtre pour retrouver l'entrée LDAP associée à son compte.
    * Ceci laisse aussi sous-entendre que le mot de passe est également stocké en clair dans le répertoire LDAP.
    * Le mot de passe pourrait être imprimé par un _logger_ (journalisation) accidentellement, soit dans le code ou par un intercepteur de requêtes, comme on en retrouve souvent.
* Solution:
    * Utiliser un autre champ pour retrouver l'entrée LDAP associée.
    * Encrypter le mot de passe dans le répertoire LDAP.
    * Déléguer l'authentification à un service externe (Kerberos, SAML).
