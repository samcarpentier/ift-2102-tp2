#!/bin/bash

mkdir -p target
pandoc --standalone \
  src/title_page.md \
  src/config.md \
  src/report.md \
--output target/report.pdf
