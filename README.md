# ift-2102-tp2 [![pipeline status](https://gitlab.com/samcarpentier/ift-2102-tp2/badges/master/pipeline.svg)](https://gitlab.com/samcarpentier/ift-2102-tp2/commits/master)

To download the latest generated PDF, click [here](https://gitlab.com/samcarpentier/ift-2102-tp2/-/jobs/artifacts/master/raw/report.pdf?job=create_pdf_artifact)
